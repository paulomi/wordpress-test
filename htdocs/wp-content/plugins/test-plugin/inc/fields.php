<?php
function employee_details_post_type() {
	$labels = array(
		'name'               => __( 'Employee Details' ),
		'singular_name'      => __( 'Employee Detail' ),
		'add_new'            => __( 'Add New Employee' ),
		'add_new_item'       => __( 'Add New Employee' ),
		'edit_item'          => __( 'Edit Employee Detail' ),
		'new_item'           => __( 'Add New Employee' ),
		'view_item'          => __( 'View Employee Detail' ),
		'search_items'       => __( 'Search Employee Detail' ),
		'not_found'          => __( 'No employee details found' ),
		'not_found_in_trash' => __( 'No employee details found in trash' )
	);
	$supports = array(
        'title',
		'page-attributes'
	);
	$args = array(
		'labels'               => $labels,
		'supports'             => $supports,
        'public'               => false,  
        'publicly_queryable'   => true,  
        'show_ui'              => true,
        'exclude_from_search'  => true,
        'show_in_nav_menus'    => false, 
        'has_archive'          => false,
        'rewrite'              => false, 
		'capability_type'      => 'post',
		'has_archive'          => true,
		'menu_position'        => 30,
		'menu_icon'            => 'dashicons-calendar-alt',
		'register_meta_box_cb' => 'add_employee_init',
	);
	register_post_type( 'employee_details', $args );
}
add_action( 'init', 'employee_details_post_type' );

function add_employee_init($post) {
    add_meta_box(
        'wpt_employee_fullname', 
        'Employee Name', 
        'wpt_employee_fullname', 
        'employee_details', 
        'normal', 
        'high'
    );
    add_meta_box(
        'wpt_employee_description', 
        'Employee Description', 
        'wpt_employee_description', 
        'employee_details', 
        'normal', 
        'high'
    );

    add_meta_box(
        'wpt_employee_image', 
        'Employee Image', 
        'wpt_employee_image', 
        'employee_details', 
        'normal', 
        'high'
    );

    add_meta_box(
        'wpt_employee_position', 
        'Employee position', 
        'wpt_employee_position', 
        'employee_details', 
        'normal', 
        'high'
    );

    add_meta_box(
        'wpt_employee_social_links', 
        'Social Network Links', 
        'wpt_employee_social_links', 
        'employee_details', 
        'normal', 
        'low'
    );
  }

  add_action('add_meta_boxes','add_employee_init');

  function wpt_employee_fullname($post) {
    ?>
     <p class="first_name">
            <label for="first_name">First Name</label>
            <input id="first_name"
                type="text"
                name="first_name"
                value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'first_name', true ) ); ?>">
        </p>
        <p class="last_name">
            <label for="last_name">Last Name</label>
            <input id="last_name"
                type="text"
                name="last_name"
                value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'last_name', true ) ); ?>">
        </p>
    
<?php
  }
  add_action('save_post','save_employee_name');
function save_employee_name($post_id) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( $parent_id = wp_is_post_revision( $post_id ) ) {
        $post_id = $parent_id;
    }
    $names = [
        'first_name',
        'last_name'
    ];
    foreach ( $names as $name ) {
        if ( array_key_exists( $name, $_POST ) ) {
            update_post_meta( $post_id, $name, sanitize_text_field( $_POST[$name] ) );
        }
     }
}
  function wpt_employee_description($post){

    $txtEmployeeDescription = get_post_meta($post->ID, 'txtEmployeeDescription', true);
?>
        <label for="txtEmployeeDescription">Please add a description</label>
    	<br>
    	<textarea name="txtEmployeeDescription" id="txtEmployeeDescription" rows="5" cols="30" style="width:100%;">
        <?php echo $txtEmployeeDescription; ?></textarea>
<?php
}

add_action('save_post','save_employee_description');
function save_employee_description(){
    global $post;
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if(isset($_POST["txtEmployeeDescription"])) {
    $txtEmployeeDescription = $_POST['txtEmployeeDescription'];
    update_post_meta( $post->ID, 'txtEmployeeDescription', $txtEmployeeDescription);
    }
}

//second metabox
// function add_employee_metabox_two($post) {
    
//   }
//   add_action('add_meta_boxes','add_employee_metabox_two');
  function wpt_employee_image() {
	wp_nonce_field( plugin_basename(__FILE__), 'wpt_employee_image_nonce' );
	$html = '<p class="description">Upload Employee Image Here.</p>';
	$html .= '<input id="wpt_employee_image" name="wpt_employee_image" size="25" type="file" value="" accept="image/png, image/jpeg"/>';

	$filearray = get_post_meta( get_the_ID(), 'wpt_employee_image', true );
	$this_file = $filearray['url'];
	
	if ( $this_file != '' ) { 
	     $html .= '<div><p>Current file: ' . $this_file . '</p></div>'; 
	}
	echo $html; 
}

function save_employee_image( $id ) {
	if ( ! empty( $_FILES['wpt_employee_image']['name'] ) ) {
		$supported_types = array( 'image/png', 'image/jpeg' );
		$arr_file_type = wp_check_filetype( basename( $_FILES['wpt_employee_image']['name'] ) );
		$uploaded_type = $arr_file_type['type'];

		if ( in_array( $uploaded_type, $supported_types ) ) {
			$upload = wp_upload_bits($_FILES['wpt_employee_image']['name'], null, file_get_contents($_FILES['wpt_employee_image']['tmp_name']));
			if ( isset( $upload['error'] ) && $upload['error'] != 0 ) {
				wp_die( 'There was an error uploading your file. The error is: ' . $upload['error'] );
			} else {
				add_post_meta( $id, 'wpt_employee_image', $upload );
				update_post_meta( $id, 'wpt_employee_image', $upload );
			}
		}
		else {
			wp_die( "The file type that you've uploaded is not an image." );
		}
	}
}
add_action( 'save_post', 'save_employee_image' );

function update_edit_form() {
	echo ' enctype="multipart/form-data"';
}
add_action( 'post_edit_form_tag', 'update_edit_form' );

//metabox three
  function wpt_employee_position($post){

    $employeePosition = get_post_meta($post->ID, 'wpt_employee_position', true);
?>

    <label>Choose Employee position :  </label>
    <select name="employee-position-class" id="employee-position-class">
        <option value="ceo" <?php selected( $employeePosition, 'CEO' ); ?>>CEO</option>
        <option value="Project Manager"<?php selected( $employeePosition, 'Project Manager' ); ?>>Project Manager</option>
        <option value="Developer"<?php selected( $employeePosition, 'Developer' ); ?>>Developer</option>
    </select>
           
<?php
}
add_action('save_post','save_employee_position');
function save_employee_position(){
    global $post;
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if(isset($_POST["employee-position-class"])) {
        $employeePosition = $_POST['employee-position-class'];
        update_post_meta( $post->ID, 'wpt_employee_position', $employeePosition);
    }       
}

function wpt_employee_social_links($post) {
    $socialLinks = get_post_meta($post->ID, 'socialLinks', true);
    ?>
    <div>
        <p class="link_one">
            <label for="social_link_one">Github</label>
            <input id="social_link_one"
                type="text"
                name="social_link_one"
                value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'social_link_one', true ) ); ?>">
        </p>
        <p class="link_two">
            <label for="social_link_two">Linkedin</label>
            <input id="social_link_two"
                type="text"
                name="social_link_two"
            value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'social_link_two', true ) ); ?>">
        </p>
        <p class="link_three">
            <label for="social_link_three">Xing</label>
            <input id="social_link_three"
                type="text"
                name="social_link_three"
                value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'social_link_three', true ) ); ?>">
        </p>
        <p class="link_four">
            <label for="social_link_four">Facebook</label>
            <input id="social_link_four"
                type="text"
                name="social_link_four"
                value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'social_link_four', true ) ); ?>">
        </p>
    </div>
<?php
}

function social_links_save_meta_box( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( $parent_id = wp_is_post_revision( $post_id ) ) {
        $post_id = $parent_id;
    }
    $fields = [
        'social_link_one',
        'social_link_two',
        'social_link_three',
        'social_link_four'
    ];
    foreach ( $fields as $field ) {
        if ( array_key_exists( $field, $_POST ) ) {
            update_post_meta( $post_id, $field, sanitize_text_field( $_POST[$field] ) );
        }
     }
}
add_action( 'save_post', 'social_links_save_meta_box' );

    
