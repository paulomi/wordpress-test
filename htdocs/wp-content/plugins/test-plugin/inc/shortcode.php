<?php
add_shortcode( 'shortcodename', 'my_employee_details_post_type' );

function my_employee_details_post_type($atts) {
    global $post; 
    $meta = get_post_meta(get_the_ID(), 'first_name', true); 
    $args = array(
        'post_type' => 'employee_details',
        'post_status' => 'publish',
        'name' => $meta
    );

    $string = '';
    $query = new WP_Query( $args );
    if( $query->have_posts() ) {
        $string .= '<div class="cointainer">';
        while( $query->have_posts() ) {
            $query->the_post();  
            $meta = get_post_meta(get_the_ID(), 'first_name', true); 
            ?>
            <div class="employee-info">
                <?php
                if ( get_post_meta( get_the_ID(), 'wpt_employee_image', true ) ) {
                    $image_url = get_post_meta( get_the_ID(), 'wpt_employee_image', true );
                } else {
                    $image_url = ''; // You can put the fallback featured image code here if you want.
                }
                ?>
                <img src="<?php echo $image_url['url'] ?>">
            <p class="name"> 
            
                <?php echo  esc_attr( get_post_meta( get_the_ID(), 'first_name', true ) )  ?>
                <?php echo esc_attr( get_post_meta( get_the_ID() , 'last_name', true ) ) ?></p>
           
                <?php 
                $select = get_post_meta(get_the_ID(), 'wpt_employee_position', true); 
                ?>
                <?php 
                switch ( $select ) {
                    case 'ceo':
                        echo 'CEO';
                        break;
                    case 'Project Manager':
                        echo 'Project Manager';
                        break;
                    case 'Developer':
                        echo 'Developer';
                        break;
                    default:
                        echo 'No option selected';
                        break;
                } 
                $name = get_post_meta( get_the_ID(), 'first_name', true);
            ?> 
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal--<?php echo $name; ?>">More Info</button>
            <div class="modal fade" id="myModal--<?php echo $name; ?>" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <p class="name"> 
                            <?php echo  esc_attr( get_post_meta( get_the_ID(), 'first_name', true ) )  ?>
                            <?php echo esc_attr( get_post_meta( get_the_ID() , 'last_name', true ) ) ?>
                            </p>
                        </div>
                        <div class="modal-body">
                            <strong>Description: </strong>
                            <p><?php echo esc_attr( get_post_meta( get_the_ID(),'txtEmployeeDescription', true ) ) ?> </p>
                            <strong>Social Network Links: </strong>
                            <ul>
                            <li><strong>Github: </strong><?php echo  esc_attr( get_post_meta( get_the_ID(), 'social_link_one', true ) )  ?></li>
                            <li><strong>Linkedin: </strong><?php echo  esc_attr( get_post_meta( get_the_ID(), 'social_link_two', true ) )  ?></li>
                            <li><strong>Xing: </strong><?php echo  esc_attr( get_post_meta( get_the_ID(), 'social_link_three', true ) )  ?></li>
                            <li><strong>Facebook: </strong><?php echo  esc_attr( get_post_meta( get_the_ID(), 'social_link_four', true ) )  ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        
        </div>
            
           

    <?php }
        $string .= '</div>';
    }
    wp_reset_postdata();
    return $string;
    
}
