<?php
/** 
 * @wordpress-plugin
 * Plugin Name: MY TEST PLUGIN
 * Plugin URI:        https://paulomiatcodecorner.co.uk
 * Description:       wordpress plugin
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Paulomi Bhadra
 * Author URI:        https://paulomiatcodecorner.co.uk
 * Text Domain:       test-plugin
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */


/**
 * Registers the event post type.
 */

 //plugin styles
 add_action('wp_enqueue_scripts','load_my_styles');

 function load_my_styles(){
     wp_enqueue_style( 'stylesheet', plugins_url( './test-plugin.css' , __FILE__ ) );
     wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.4.1.min.js');
     wp_enqueue_script("bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css");
     wp_enqueue_script('bootstrap-js', "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js");
     wp_enqueue_script( 'js', plugins_url( './test-plugin.js' , __FILE__ ) );
 }

 //plugin fields
include plugin_dir_path( __FILE__ ) . './inc/fields.php';

//plugin shortcode
include plugin_dir_path( __FILE__ ) . './inc/shortcode.php';
