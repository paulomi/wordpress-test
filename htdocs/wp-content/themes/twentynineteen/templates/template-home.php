<?php
/**
 * Template Name: home
 */
?>

<div class="home__page">
    <?php
        if(have_posts()) {
            while (have_posts()) {
                the_post();
                get_template_part('page');
                echo do_shortcode('[shortcodename]');
            } 
            wp_reset_postdata();
        }x

       
    ?>
</div>